<?php

// Create Custome Setting for your Theme.
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Sidebar Custom Settings',
		'menu_title'	=> 'Sidebar Settings',
		'menu_slug' 	=> 'sidebar-custom-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
    ));
    
}
// END

// CREATE CUSTOMIZER
include(get_template_directory() .'/templat_parts/customizer.php');
// END

function load_stylesheets()
{
    // wp_register_style( string $handle, string|bool $src, array $deps = array(), string|bool|null $version = false, string $media = 'all' );

    wp_register_style('mystylesheet', get_template_directory_uri() . '/style.css', array(), null, 'all');
    wp_enqueue_style('mystylesheet');
}
add_action('wp_enqueue_scripts', 'load_stylesheets');

function load_slickstyle()
{
    // wp_register_style( string $handle, string|bool $src, array $deps = array(), string|bool|null $version = false, string $media = 'all' );

    wp_register_style('myslickstyle', get_template_directory_uri() . '/assets/slick-1.8.1/slick/slick.css', array(), null, 'all');
    wp_enqueue_style('myslickstyle');
}
add_action('wp_enqueue_scripts', 'load_slickstyle');

function load_slickthemestyle()
{
    // wp_register_style( string $handle, string|bool $src, array $deps = array(), string|bool|null $version = false, string $media = 'all' );
    wp_register_style( 'Font_Awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css' );
    wp_enqueue_style('Font_Awesome');
 
    wp_register_style('myslickthemestyle', get_template_directory_uri() . '/assets/slick-1.8.1/slick/slick-theme.css', array(), null, 'all');
    wp_enqueue_style('myslickthemestyle');
}
add_action('wp_enqueue_scripts', 'load_slickthemestyle');


function loadjquery()
{
    // wp_register_script( string $handle, string|bool $src, array $deps = array(), string|bool|null $version = false, bool $in_footer = false );
    
    wp_register_script('customjquery', 'https://code.jquery.com/jquery-2.2.0.min.js', $deps = array('jquery'), null, true);
    wp_enqueue_script('customjquery');
}
add_action('wp_enqueue_scripts', 'loadjquery');

function loadjs()
{
    // wp_register_script( string $handle, string|bool $src, array $deps = array(), string|bool|null $version = false, bool $in_footer = false );
    
    wp_register_script('customjs', get_template_directory_uri(). '/assets/js/scripts.js', '', null, true);
    wp_enqueue_script('customjs');
}
add_action('wp_enqueue_scripts', 'loadjs');


function loadslick()
{
    // wp_register_script( string $handle, string|bool $src, array $deps = array(), string|bool|null $version = false, bool $in_footer = false );
    
    wp_register_script('slickimport', get_template_directory_uri(). '/assets/slick-1.8.1/slick/slick.js', '', null, true);
    wp_enqueue_script('slickimport');
}
add_action('wp_enqueue_scripts', 'loadslick');

add_theme_support('menus');

// ADD SUPPORT FOR FEATURED IMAGE
add_theme_support('post-thumbnails');

add_image_size( 'smallesth', 100, 50, true );
add_image_size( 'archive', 320, 250 );

register_nav_menus(
    array(
        'top-menu' => __('Top Menu', 'theme'),
        'footer-menu' => __('Footer Menu', 'theme')
    )
);

// FOR AJAX YOUTUBE VIDEO FUNCTIONALITY
function load_ajax() 
{
    wp_enqueue_script( 'ajax-script', get_template_directory_uri() . '/assets/js/my_ajax_script.js', array('jquery'), null, true );
    wp_localize_script( 'ajax-script', 'my_ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
}
add_action( 'wp_enqueue_scripts', 'load_ajax' );

add_action( 'wp_ajax_ajax_youtube_action', 'ajax_youtube_action' );
add_action( 'wp_ajax_nopriv_ajax_youtube_action', 'ajax_youtube_action' );

function ajax_youtube_action() 
{   
    $youtube_url = $_GET['childcount'];
    if ( strlen($youtube_url) > 11 ) {
        $value = explode("watch?v=", $youtube_url);
        $youtube_url2 = $value[1];
    } else {
        $youtube_url2 =  $youtube_url;
    }
    echo $youtube_url2;
    wp_die();
}
// END


// SET ACF IMAGE AS FEATURED IMAGE
add_filter( 'save_post', 'set_thumbnail' );
function set_thumbnail()
{
    $abc = get_field_object('event_banner');
    $thumbnail_id = attachment_url_to_postid($abc['value']);
    $post_id = get_the_ID();
    set_post_thumbnail($post_id, $thumbnail_id);
}
// END

// FILTER NAME OF IMAGE AND CHANGE IT
// add_filter('wp_handle_upload_prefilter', 'custom_upload_filter' );

// function custom_upload_filter( $file )  {
//     // $file['name'] = 'wp-is-awesome-' . $file['name'];
//     var_dump($file);
//     die("error herew");
//     // return $file;
// }
// END

// ROTATE IMAGE AND SAVE IT
// add_filter('wp_handle_upload_prefilter', 'custom_rotate_filter' );

// function custom_rotate_filter( $file )  
// {
//     // for ($i = 2 ; $i < 4; $i++ )
//     // {
//         // $file['name'] = $i .'-copy'.$file['name'];
//         return createimage($file);
//     // }
// }

// add_filter('wp_handle_upload_prefilter', 'createimage' );
// function createimage($file) 
// {
//     var_dump($file);
    
//     $myfiles = array();
    
//     for ($i = 1 ; $i < 4; $i++ )
//     {
//         $file['name'] = $i .'-copy'.$file['name'];
//         $file['tmp_name'] =  $file['tmp_name'] . $i;
//     //     return $file;
//     //    return $file;
//         array_push($myfiles, $file);
//         // var_dump($myfiles[$i]);
//         // return $myfiles[$i];
//     }
//     var_dump($myfiles);
//     return $myfiles;
//     // wp_handle_upload($file);
//     // die();
    
// } 

add_filter('wp_handle_upload', 'custom_upload_filter' );
function custom_upload_filter( $file )
{
    // var_dump($file);
    // $exif = new Imagick( $file['file'] );
    $exif = exif_read_data( $file['file'] );
    var_dump($exif);
    $image_editor = wp_get_image_editor($file['file']);
    // var_dump($image_editor);
    // $image_editor->resize( 200, 200, true );
    $image_editor->rotate( 90 );
    $image_editor->save($file['file']);
    // return $file;
    die();
}

// EXCERPT WORD LENGTH TO SHOW
add_filter( 'excerpt_length', function($length) {
    return 20;
} );

// Events Custom Post Type
function events_init() {
    // set up event labels
    $labels = array(
        'name' => 'Events',
        'singular_name' => 'Event',
        'add_new' => 'Add New Event',
        'add_new_item' => 'Add New Event',
        'edit_item' => 'Edit Event',
        'new_item' => 'New Event',
        'all_items' => 'All Events',
        'view_item' => 'View Events',
        'search_items' => 'Search Events',
        'not_found' =>  'No Events Found',
        'not_found_in_trash' => 'No Events found in Trash', 
        'parent_item_colon' => '',
        'menu_name' => 'Events',
    );
    
    // register post type
    $args = array(
        'labels' => $labels,
        'public' => true,
        'has_archive' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => array('slug' => 'events'),
        'query_var' => true,
        'menu_icon' => 'dashicons-star-empty',
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'trackbacks',
            'custom-fields',
            'comments',
            'revisions',
            'thumbnail',
            'author',
            'page-attributes'
        )
    );
    register_post_type( 'events', $args );
    
    // register taxonomy
    // register_taxonomy( string $taxonomy, array|string $object_type, array|string $args = array() )
    register_taxonomy('event_category', 'events', array('hierarchical' => true, 'label' => 'event_category', 'query_var' => true, 'rewrite' => array( 'slug' => 'event-category' )));
}
add_action( 'init', 'events_init' );

// Speaker Custom Post Type
function speakers_init() {
    // set up Speaker labels
    $labels = array(
        'name' => 'Speakers',
        'singular_name' => 'Speaker',
        'add_new' => 'Add New Speaker',
        'add_new_item' => 'Add New Speaker',
        'edit_item' => 'Edit Speaker',
        'new_item' => 'New Speaker',
        'all_items' => 'All Speakers',
        'view_item' => 'View Speakers',
        'search_items' => 'Search Speaker',
        'not_found' =>  'No Speakers Found',
        'not_found_in_trash' => 'No Speakers found in Trash', 
        'parent_item_colon' => '',
        'menu_name' => 'Speakers',
    );
    
    // register post type
    $args = array(
        'labels' => $labels,
        'public' => true,
        'has_archive' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => array('slug' => 'speakers'),
        'query_var' => true,
        'menu_icon' => 'dashicons-megaphone',
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'trackbacks',
            'custom-fields',
            'comments',
            'revisions',
            'thumbnail',
            'author',
            'page-attributes'
        )
    );
    register_post_type( 'speakers', $args );
}
add_action( 'init', 'speakers_init' );

// Events Sponsor Post Type
function sponsors_init() {
    // set up Sponsor labels
    $labels = array(
        'name' => 'Sponsors',
        'singular_name' => 'Sponsor',
        'add_new' => 'Add New Sponsor',
        'add_new_item' => 'Add New Sponsor',
        'edit_item' => 'Edit Sponsor',
        'new_item' => 'New Sponsor',
        'all_items' => 'All Sponsors',
        'view_item' => 'View Sponsors',
        'search_items' => 'Search Sponsors',
        'not_found' =>  'No Sponsors Found',
        'not_found_in_trash' => 'No Sponsors found in Trash', 
        'parent_item_colon' => '',
        'menu_name' => 'Sponsors',
    );
    
    // register post type
    $args = array(
        'labels' => $labels,
        'public' => true,
        'has_archive' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => array('slug' => 'sponsors'),
        'query_var' => true,
        'menu_icon' => 'dashicons-tickets',
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'trackbacks',
            'custom-fields',
            'comments',
            'revisions',
            'thumbnail',
            'author',
            'page-attributes'
        )
    );
    register_post_type( 'sponsors', $args );
}
add_action( 'init', 'sponsors_init' );

// Video Custom Post Type
function videos_init() {
    // set up Video labels
    $labels = array(
        'name' => 'Videos',
        'singular_name' => 'Video',
        'add_new' => 'Add New Video',
        'add_new_item' => 'Add New Video',
        'edit_item' => 'Edit Video',
        'new_item' => 'New Video',
        'all_items' => 'All Videos',
        'view_item' => 'View Videos',
        'search_items' => 'Search Video',
        'not_found' =>  'No Videos Found',
        'not_found_in_trash' => 'No Videos found in Trash', 
        'parent_item_colon' => '',
        'menu_name' => 'Videos',
    );
    
    // register post type
    $args = array(
        'labels' => $labels,
        'public' => true,
        'has_archive' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => array('slug' => 'videos'),
        'query_var' => true,
        'menu_icon' => 'dashicons-video-alt3',
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'trackbacks',
            'custom-fields',
            'comments',
            'revisions',
            'thumbnail',
            'author',
            'page-attributes'
        )
    );
    register_post_type( 'videos', $args );
}
add_action( 'init', 'videos_init' );
