<?php get_header(); ?>

<div class="video_container">
    <?php 
    $myquery = new WP_Query(array(
        'post_type' => 'videos',
        'posts_per_page' => 1,
        // 'order' => 'ASC',
    ));

    while ( $myquery->have_posts() ) : $myquery->the_post();
        $myvideourl = get_field('youtube_url');
    endwhile;
    $myrealvideo = $myvideourl;

    if ( strlen($myrealvideo) > 11 ) {
        $value = explode("watch?v=", $myrealvideo);
        $imageurl = $value[1];
    } else {
        $imageurl = $myrealvideo;
    }
    wp_reset_query();
    ?>

    <div class="video_player">
        <img class="youtube_thumbnail" src="https://img.youtube.com/vi/<?= $imageurl; ?>/hqdefault.jpg" data-attr2="<?= $imageurl; ?>" alt="Youtube Thumbnail">
       <div id="youtube_player"></div>
    </div>

    <ul class="video_thumbnails">
        <?php
        $query = new WP_Query(array(
            'post_type' => 'videos',
        ));

        if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();            
            $url = get_field('youtube_url');
        ?>
        <li class="banner_slider_li video_list" data-attr="<?= $url ?>">
            <?php 
            if ( strlen($url) > 11 ) {
                $value2 = explode("v=", $url);
                $videoId = $value2[1];
            } else { $videoId = $url; }
            ?>
            <img class="youtube_thumbnail_image" src='https://img.youtube.com/vi/<?= $videoId; ?>/hqdefault.jpg' alt='hello'>
            <h4><?php the_title(); ?></h4>
        </li>
        <?php endwhile; endif; wp_reset_query();?>
    </ul>
</div>
<?php get_footer(); ?>