<!DOCTYPE html>
<html>
    <head>

        <?php wp_head(); ?>

        <style>
            header, footer, .homepage_sidebar {
                background-color: <?= get_theme_mod('change_color_control') ?>;
            }
        </style>

    </head>
<body <?php body_class(); ?>>

<header>
    <div class="wrapper">
        <h1>
            <a href="http://localhost/rahul/wordpress/" title="<?= get_theme_mod('change_title_control') ?>"><?= get_theme_mod('change_title_control') ?></a>
        </h1>

        <div class="mymenus">

            <?php wp_nav_menu(
                array(
                    'theme_location' => 'top-menu',
                    'menu_class' => 'navigation'
                )
            ); ?>

        </div>

        <ul class="header_right">
            <li><span>follow</span><span class="menu-down">down</span></li>
            <li><span class="search">search</span></li>
        </ul>
    </div>

</header>