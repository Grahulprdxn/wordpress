<span><b>Speaker's Organization: </b></span>
<?php the_field('speaker_organization'); ?>
<br>
<br>
<span><b>Speaker's Description: </b></span>
<?php the_field('speaker_description'); ?>
<br>
<br>
<span><b>Speaker's Designation: </b></span>
<?php the_field('speaker_designation'); ?>