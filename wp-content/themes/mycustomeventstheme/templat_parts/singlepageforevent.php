<figure>
    <img class="singlebannerimage" src="<?= the_field('event_banner') ?>">
</figure>

<?php var_dump(the_field('event_banner')); ?>

<?php var_dump(get_the_ID()); ?>

<!-- DISPLAY THE SPONSORS -->
<?php get_template_part( '/templat_parts/displaycategories' ); ?>

<!-- DISPLAY EVENT DATE & TIME -->
<span><b>Date & Time: </b></span>
<?php the_field('event_date'); ?>
<br>
<br>

<!-- DISPLAY EVENT LOCATION -->
<span><b>location: </b></span>
<?php $location = get_field('event_location'); var_dump($location);?>
<br>
<br>

<!-- DISPLAY EVENT DESCRIPTION -->
<span><b>Event Description: </b></span>
<?php the_field('event_description'); ?>
<br>
<br>

<!-- DISPLAY EVENT ENQUIRY -->
<?php the_field('inquiry'); ?>
<br>
<br>


<!-- DISPLAY SPEAKERS -->
<span>Our Speakers are: </span>
<?php $speakers = get_field('speakers');
    foreach ($speakers as $speaker) :
?>
<a href="<?= get_permalink( $speaker->ID ); ?>"><span><?= get_the_title( $speaker->ID ); ?> </span></a>
<?php endforeach; ?>

<!-- DISPLAY SIMILAR EVENTS -->
<?php $similarevents = get_field('similar_events');?>
<p>Similar Events: </p>
<ul class="similar_events" style="display:flex; flex-wrap:wrap; list-style:none; justify-content: space-between;">
    <?php foreach ($similarevents as $similarevent) :?>
        <li style="flex-basis:40%;">
            <img class="singlebannerimage" src="<?= get_the_post_thumbnail_url( $similarevent->ID ); ?>">
            <a href="<?= get_permalink( $similarevent->ID ); ?>"><b><span><?= get_the_title( $similarevent->ID ); ?></b> </span></a>
        </li>
    <?php endforeach; ?>
</ul>
