<?php 
function mytheme_customize_register( $wp_customize ) {
    $wp_customize->add_panel( 'header_settings', array(
        'title' => 'Header color change',
        'description' => 'background color change of header',
        'priority' => 10,
    ) );

    // FOR HEADER COLOR STARTS HERE
    $wp_customize->add_section( 'header_color_settings', array(
        'title' => 'Colors',
        'priority' => 10,
        'panel' => 'header_settings',
    ) );

    $wp_customize->add_setting( 'change_color_control', array(
        'default'  => '#69A49E',
        // 'transport' => 'refresh',
        // 'active_callback' => 'change_color',
    ) );

    $wp_customize->add_control( 'change_color_control', array(
        'label' => __( 'header Background' ),
        'type' => 'color',
        'section' => 'header_color_settings',
    ) );
    // FOR HEADER COLOR ENDS HERE

    // FOR HEADER TITLE STARTS HERE
    $wp_customize->add_section( 'header_title_settings', array(
        'title' => 'Header title',
        'priority' => 20,
        'panel' => 'header_settings',
    ) );
    
    $wp_customize->add_setting( 'change_title_control', array(
        'default'  => 'My Custom Events',
        // 'transport' => 'refresh',
    ) );

    $wp_customize->add_control( 'change_title_control', array(
        'label' => __( 'header Title' ),
        'type' => 'text',
        'section' => 'header_title_settings',
    ) );
    // FOR HEADER TITLE ENDS HERE

 }
 add_action( 'customize_register', 'mytheme_customize_register' );
 ?>