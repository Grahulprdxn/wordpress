<div class="container">

    <!-- Here it starts -->
    <ul class="regular">
        <?php 
            $query = new WP_Query(array(
                'post_type' => 'events'
            ));
            if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
        <li class="banner_slider_li">
            <div>
                <img class="banner_slider_image" src="<?= the_post_thumbnail_url(); ?>" alt="Banner Image">
                <a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>                
            </div>
        </li>
        <?php endwhile; endif; ?>
    </ul>
    <!-- here it ends -->

</div>