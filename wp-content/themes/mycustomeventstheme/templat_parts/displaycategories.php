<?php if (get_field('is_private')) : ?>
    <p>private</p>
    <p>Our Sponsors: </p>
    <ul class="spaonsor_display">
        <?php $posts = get_field('sponsors'); 
            foreach ($posts as $mypost) : ?>
            <li>
                <a href="<?= get_permalink( $mypost->ID );; ?>"><h4><?= get_the_title( $mypost->ID ) ?></h4></a>
            </li>
        <?php endforeach ?>
    </ul>
<?php endif; ?>