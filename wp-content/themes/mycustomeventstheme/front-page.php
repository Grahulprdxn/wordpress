<?php get_header(); ?>

<!-- Get breaking News from templat_parts folder -->
<?php get_template_part( '/templat_parts/breakingnewsbanner' ); ?>

<!-- DISPLAY SPEAKERS -->
<div class="homepage_sidebar">
    <img src="<?php the_field('profile_image', 'option'); ?>" alt="Sidebar Image">
    <h4><?php the_field('profile_name', 'option'); ?></h4>
    <p><?php the_field('profile_description', 'option'); ?></p>
</div>

<?php get_footer(); ?>