var dataAttr2, player;

$( document ).ready(function() {

    var lists = document.querySelectorAll(".video_list");
    console.log(lists);

    for(var i = 0; i < lists.length; i++) {
        lists[i].addEventListener("click", loadthumbnail);
    }

    // Ajax For loading recipes filter
    function loadthumbnail() {
        var thisvalue = this.getAttribute("data-attr");
        $(".youtube_thumbnail").show();
        // $(".video_player iframe").remove();
        $(".video_player iframe").hide();
        // $(".video_player iframe").attr("src", "");

        $.ajax({
            type: "get",
            url: my_ajax_object.ajax_url,
            data: {
                childcount: thisvalue,
                action: 'ajax_youtube_action',
            },
            success: function(result) {
                var writeUrl = "https://img.youtube.com/vi/" + result + "/hqdefault.jpg";
                $(".youtube_thumbnail").attr("src", writeUrl);
                $(".youtube_thumbnail").attr("data-attr2", result);
            }
        });
    }

    $(".youtube_thumbnail").click(function() {
        var imgattr = $(this).attr("data-attr2");
        $(".video_player iframe").show();
        $(".youtube_thumbnail").hide();

        $.ajax({
            type: "get",
            url: my_ajax_object.ajax_url,
            data: {
                childcount: imgattr,
                action: 'ajax_youtube_action',
            },
            success: function(result) {
                var tag = document.createElement('script');
                tag.src = "https://www.youtube.com/iframe_api";
                $(".video_player").append(tag);
                dataAttr2 = result;
                var writeUrl2 = "https://www.youtube.com/embed/" + result + "?autoplay=1";
                $(".video_player iframe").attr("src", writeUrl2);
            }
        });
    });
});



function onYouTubeIframeAPIReady() {
    player = new YT.Player('youtube_player', {
    height: '390',
    width: '640',
    videoId: dataAttr2,
    events: {
        'onReady': onPlayerReady,
    }
    });
}

function onPlayerReady(event) {
    event.target.playVideo();
} 

function stopVideo() {
    player.stopVideo();
}