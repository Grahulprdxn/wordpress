window.onload = function() {

$(".regular").slick({
    dots: true,
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1
});

$(".variable").slick({
    dots: true,
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1
});

// ADD CLASS TO MYMENUS CHILD DIV
document.querySelector(".mymenus div").classList.add("menudiv");

var lists = document.querySelectorAll(".recipe_button");

for(var i = 0; i < lists.length; i++) {
    lists[i].addEventListener("click", loadrecipe);
}

// Ajax For loading recipes filter
function loadrecipe() {
    // console.log(this.value);
    // alert(this.value);
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // document.querySelector(".recipes_ul").innerHTML = this.responseText;
            // console.log(this.responseText)
            // alert(this.responseText);
        }
        
    };
    xhttp.open("GET", "?recipe_passed=" + this.value , true);
    xhttp.send();
}
// loadrecipe("Bye");

};