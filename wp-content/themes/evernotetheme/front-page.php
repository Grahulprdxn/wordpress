<?php get_header(); ?>

<!-- BANNER SECTION STARTS HERE -->
<section class="banner">
    <figure>
        <img src="<?= get_field('banner')["banner_background_image"] ?>" alt="Banner Background Image">
    </figure>
    <div class="wrapper">
        <div class="banner_left">
            <h2><?= get_field('banner')["banner_title"] ?></h2>
            <p><?= get_field('banner')["banner_content"] ?></p>
            <a href="#FIXME" title="Sign Up For Free">sign up for free</a>
        </div>
        <figure class="banner_right">
            <img src="<?= get_field('banner')["banner_image"] ?>" alt="Banner Image">
        </figure>
    </div>
</section>
<!-- BANNER SECTION ENDS HERE -->

<!-- FLEXIBLE CONTENT STARTS HERE -->

<?php
    if( have_rows('flexible_content_evernote') ):
        while ( have_rows('flexible_content_evernote') ) : the_row();
            switch( get_row_layout() ) {
                case 'focus_on_what_matters_most':
                get_template_part('template_parts/content','focus');
                break;
                case 'at_work':
                get_template_part('template_parts/content','at_work');
                break;
                case 'evernote_heart':
                get_template_part('template_parts/content','heart');
                break;
                case 'how_it_works':
                get_template_part('template_parts/content','how_it_works');
                break;
                case 'popular_features':
                get_template_part('template_parts/content','popular_features');
                break;
                case 'sign_up_form':
                get_template_part('template_parts/content','sign_up_form');
                break;
            }
        endwhile;
    endif;
?>

<!-- FLEXIBLE CONTENT ENDS HERE -->

<?php get_footer(); ?>