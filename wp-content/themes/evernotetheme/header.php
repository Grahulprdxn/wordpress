<!DOCTYPE html>
<html>
    <head>

        <?php wp_head(); ?>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <style>
            header, footer, .homepage_sidebar {
                /* background-color: <?= get_theme_mod('change_color_control') ?>; */
            }
        </style>

    </head>
<body <?php body_class(); ?>>

<header>
    <div class="wrapper">
        <div class="header_left">
            <h1>
                <img src='<?= get_theme_mod('image_control_one') ?>' alt="Evernote">
            </h1>
            <div class="mymenus">
                <?php wp_nav_menu(
                    array(
                        'theme_location' => 'top-menu',
                        'menu_class' => 'navigation',
                        'container' => 'ul'
                    )
                ); ?>
            </div>
        </div>

        <div class="header_right">
            <a class="signup" href="<?= get_site_url()."/sign-up" ?>" title="Sign Up">Sign up</a>
            <span>or</span>
            <a class="login" href="<?= get_site_url()."/?page_id=902" ?>" title="Log In">Log In</a>
        </div>
    </div>
    <div class="wrapper search_section">
        <?php include(get_template_directory()."/searchform.php"); ?>
    </div>

</header>