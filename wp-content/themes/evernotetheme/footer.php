<?php wp_footer(); ?>
<footer>
  <div class="wrapper">
    <h3>
      <figure>
        <img src='<?= get_theme_mod('image_control_one') ?>' alt="Evernote">
      </figure>
    </h3>
    <div class="footer_middle">
      <div class="footer_middle_left">
        <div class="footer_menu_one">
          <?php wp_nav_menu(
            array(
                'theme_location' => 'footer-menu_one',
                'menu_class' => 'footer_menus',
                'container' => 'ul'
            )
          ); ?>
        </div>
        <div class="footer_menu_two">
          <?php wp_nav_menu(
            array(
                'theme_location' => 'footer-menu_two',
                'menu_class' => 'footer_menus',
                'container' => 'ul'
            )
          ); ?>
        </div>
        <div class="footer_menu_three">
          <?php wp_nav_menu(
            array(
                'theme_location' => 'footer-menu_three',
                'menu_class' => 'footer_menus',
                'container' => 'ul'
            )
          ); ?>
        </div>
        <div class="footer_menu_four">
          <?php wp_nav_menu(
            array(
                'theme_location' => 'footer-menu_four',
                'menu_class' => 'footer_menus',
                'container' => 'ul'
            )
          ); ?>
        </div>
        <div class="footer_menu_five">
          <?php wp_nav_menu(
            array(
                'theme_location' => 'footer-menu_five',
                'menu_class' => 'footer_menus',
                'container' => 'ul'
            )
          ); ?>
        </div>
      </div>
      <div class="footer_middle_right">
        <a class="signup" href="<?= get_site_url()."/?page_id=902" ?>" title="Sign Up">Sign up</a>
        <span>or</span>
        <a class="login" href="<?= get_site_url()."/?page_id=902" ?>" title="Log In">Log In</a>
      </div>
    </div>
    <div class="footer_middle_bottom">
      <span>Choose a language: English (US)</span>
      <?php wp_nav_menu(
        array(
          'theme_location' => 'footer-social',
          'menu_class' => 'footer_social'
        )
      ); ?>
    </div>
    <div class="footer_bottom">
      <p><?= get_theme_mod('change_copyright_control') ?></p>
    <?php wp_nav_menu(
        array(
          'theme_location' => 'footer-bottom_menu',
          'menu_class' => 'footer_social'
        )
      ); ?>
    </div>
  </div>
</footer>

</body>
</html>