window.onload = function() {

$(".regular").slick({
    infinite: true,
    slidesToShow: 6,
    slidesToScroll: 1,
    asNavFor: '.variable',
    focusOnSelect: true
});

$(".variable").slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    asNavFor: '.regular'
});

var social_li_count = document.querySelector(".footer_social").childElementCount;
var social_li = document.querySelectorAll(".footer_social li a");
for(var c = 0; c < social_li_count; c++){
    var social_icons = social_li[c].getAttribute("title");
    social_li[c].innerHTML = "<i class='fa fa-"+social_icons+"' aria-hidden='true'></i>";
}

$(".footer_middle_left li:first-child a").css({"color": "#000", "fontWeight": "bold", "fontSize": 12});

};
