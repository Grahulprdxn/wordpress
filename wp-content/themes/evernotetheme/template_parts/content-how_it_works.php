<section class="how_it_works">
    <div class="wrapper">
        <h3><?= get_sub_field('how_it_works_title'); ?></h3>
        <?php if( have_rows('how_it_works_repeater') ): ?>
            <ul class="hiw_content_ul">
                <?php while ( have_rows('how_it_works_repeater') ) : the_row(); ?>
                    <li>
                        <div class="hiw_content">
                            <figure>
                                <img src="<?= the_sub_field('hiw_logos') ?>" alt="HIW Logo">
                            </figure>
                            <h5><?= the_sub_field('hiw_sub_title') ?></h5>
                            <p><?= the_sub_field('hiw_sub_description');?></p>
                        </div>
                    </li>
                <?php endwhile; ?>
            </ul>
        <?php endif; ?>

        <div class="start_your_free_trial">
            <a href="#FIXME" title="Start Your Free Trial">Start Your Free Trial</a>
        </div>
    </div>
</section>