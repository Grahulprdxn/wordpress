<section class="sign_up_section">
    <div class="wrapper"> 
        <div class="signup_left">
            <h3><?= the_sub_field('sign_up_title') ?></h3>
            <p><?= the_sub_field('sign_up_description') ?></p>
        </div>
        <div class="signup_right">
            <a href="#FIXME" title="Sign up free with Google">Sign up free with Google</a>
            <p>OR</p>
            <?= do_shortcode(get_sub_field('signup_fields')) ?>
        </div>
    </div>
</section>