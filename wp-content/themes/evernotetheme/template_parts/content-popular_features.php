<section class="popular_features">
    <div class="wrapper">
        <h3><?= get_sub_field('popular_features_title'); ?></h3>
        <?php if( have_rows('popular_features_repeater') ): ?>
            <ul class="popular_features_content">
                <?php while ( have_rows('popular_features_repeater') ) : the_row(); ?>
                    <li>
                        <figure>
                            <img src="<?= the_sub_field('popular_features_logos') ?>" alt="Popular Features Logo">
                        </figure>
                        <h4><?= the_sub_field('popular_features_sub_title');?></h4>
                    </li>
                <?php endwhile; ?>
            </ul>
        <?php endif; ?>
    </div>
</section>