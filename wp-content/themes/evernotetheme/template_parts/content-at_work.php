<section class="at_work">
    <div class="wrapper">
        <figure class="at_work_logo">
            <img src="<?= get_sub_field('at_work_logo'); ?>" alt="At Work Logo">
        </figure>
        <h3><?= get_sub_field('at_work_title'); ?></h3>
        <p><?= get_sub_field('at_work_description') ?></p>

        <?php 
            $query = new WP_Query(array(
                'post_type' => 'evernotes',
                'order' => 'ASC'
            ));
            // var_dump($query);
            if ( $query->have_posts() ) :
        ?>
        <ul class="at_work_content_ul">
            <?php while ( $query->have_posts() ) : $query->the_post(); ?>
            <li>            
                <div class="at_work_content_li">
                    <figure class="blog_image" style="order:<?= the_field('image_position') ?>">
                        <img src='<?php the_post_thumbnail_url('smallest'); ?>' alt='hello'>            
                    </figure>
                    <div class="blog_content">
                        <h4><?= the_title(); ?></h4>
                        <p><?= the_field('blog_description'); ?></p>
                        <a href="<?= the_permalink(); ?>"><?= the_field('about_blog'); ?></a>
                    </div>
                </div>
            </li>
            <?php endwhile; ?>

        </ul>
            <?php endif; 
                wp_reset_postdata();
            ?>
        <div class="compare_evernote">
            <a href="#FIXME" title="Compare Evernote Plans">COMPARE EVERNOTE PLANS</a>
        </div>
    </div>
</section>