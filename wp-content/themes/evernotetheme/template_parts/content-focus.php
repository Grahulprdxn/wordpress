<section class="focus">
    <div class="wrapper">
        <figure class="focus_logo">
            <img src="<?= get_sub_field('focus_logo'); ?>" alt="Focus Logo">
        </figure>
        <h3><?= get_sub_field('focus_title'); ?></h3>

        <?php if( have_rows('foucus_content_repeater') ): ?>
            <ul class="focus_content">
                <?php while ( have_rows('foucus_content_repeater') ) : the_row(); ?>
                    <li>
                        <p><?= the_sub_field('focus_content');?></p>
                    </li>
                <?php endwhile; ?>
            </ul>
        <?php endif; ?>
    </div>
</section>