<section class="heart_section">
    <div class="wrapper">
        <figure>
            <img src="<?= the_sub_field('heart_logo'); ?>" alt="Heart Logo">
        </figure>
        <?php if( have_rows('companies_repeater') ): ?>
            <ul class="company_logos_ul regular">
                <?php while ( have_rows('companies_repeater') ) : the_row(); ?>
                    <li class="company_logos">
                        <figure>
                            <img src="<?= the_sub_field('company_logo') ?>" alt="Company Logo">
                        </figure>
                    </li>
                <?php endwhile; ?>
            </ul>
            <ul class="company_quotes_ul variable">
                <?php while ( have_rows('companies_repeater') ) : the_row(); ?>
                    <li class="company_quotes">
                        <p><q><?= the_sub_field('company_content') ?></q></p>
                    </li>
                <?php endwhile; ?>
            </ul>
        <?php endif; ?>
    </div>
</section>