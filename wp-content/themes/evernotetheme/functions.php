<?php

// CREATE CUSTOMIZER
require(get_template_directory() .'/template_parts/customizer.php');
// END

function load_stylesheets()
{
    // wp_register_style( string $handle, string|bool $src, array $deps = array(), string|bool|null $version = false, string $media = 'all' );

    wp_register_style('mystylesheet', get_template_directory_uri() . '/style.css', array(), null, 'all');
    wp_enqueue_style('mystylesheet');
}
add_action('wp_enqueue_scripts', 'load_stylesheets');

function load_slickstyle()
{
    // wp_register_style( string $handle, string|bool $src, array $deps = array(), string|bool|null $version = false, string $media = 'all' );
    wp_register_style('myslickstyle', get_template_directory_uri() . '/assets/slick-1.8.1/slick/slick.css', array(), null, 'all');
    wp_enqueue_style('myslickstyle');
}
add_action('wp_enqueue_scripts', 'load_slickstyle');

function load_slickthemestyle()
{
    // wp_register_style( string $handle, string|bool $src, array $deps = array(), string|bool|null $version = false, string $media = 'all' );
    wp_register_style( 'Font_Awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css' );
    wp_enqueue_style('Font_Awesome');
 
    wp_register_style('myslickthemestyle', get_template_directory_uri() . '/assets/slick-1.8.1/slick/slick-theme.css', array(), null, 'all');
    wp_enqueue_style('myslickthemestyle');
}
add_action('wp_enqueue_scripts', 'load_slickthemestyle');


function loadjquery()
{
    // wp_register_script( string $handle, string|bool $src, array $deps = array(), string|bool|null $version = false, bool $in_footer = false );    
    wp_register_script('customjquery', 'https://code.jquery.com/jquery-2.2.0.min.js', $deps = array('jquery'), null, true);
    wp_enqueue_script('customjquery');
}
add_action('wp_enqueue_scripts', 'loadjquery');

function loadjs()
{
    // wp_register_script( string $handle, string|bool $src, array $deps = array(), string|bool|null $version = false, bool $in_footer = false );    
    wp_register_script('customjs', get_template_directory_uri(). '/assets/js/scripts.js', '', null, true);
    wp_enqueue_script('customjs');
}
add_action('wp_enqueue_scripts', 'loadjs');


function loadslick()
{
    // wp_register_script( string $handle, string|bool $src, array $deps = array(), string|bool|null $version = false, bool $in_footer = false );    
    wp_register_script('slickimport', get_template_directory_uri(). '/assets/slick-1.8.1/slick/slick.js', '', null, true);
    wp_enqueue_script('slickimport');
}
add_action('wp_enqueue_scripts', 'loadslick');

add_theme_support('menus');

// ADD SUPPORT FOR FEATURED IMAGE
add_theme_support('post-thumbnails');

register_nav_menus(
    array(
        'top-menu' => __('Top Menu', 'theme'),
        'footer-menu_one' => __('Footer Menu One', 'theme'),
        'footer-menu_two' => __('Footer Menu Two', 'theme'),
        'footer-menu_three' => __('Footer Menu Three', 'theme'),
        'footer-menu_four' => __('Footer Menu Four', 'theme'),
        'footer-menu_five' => __('Footer Menu Five', 'theme'),
        'footer-social' => __('Footer Social', 'theme'),
        'footer-bottom_menu' => __('Footer Bottom Menu', 'theme')
    )
);

// SET ACF IMAGE AS FEATURED IMAGE
add_filter( 'save_post', 'set_thumbnail' );
function set_thumbnail()
{
    $abc = get_field_object('blog_image');
    $thumbnail_id = attachment_url_to_postid($abc['value']);
    $post_id = get_the_ID();
    set_post_thumbnail($post_id, $thumbnail_id);
}
// END

// Events Custom Post Type
function evernote_init() {
    // set up event labels
    $labels = array(
        'name' => 'Evernotes',
        'singular_name' => 'Evernote',
        'add_new' => 'Add New Evernote',
        'add_new_item' => 'Add New Evernote',
        'edit_item' => 'Edit Evernote',
        'new_item' => 'New Evernote',
        'all_items' => 'All Evernotes',
        'view_item' => 'View Evernotes',
        'search_items' => 'Search Evernotes',
        'not_found' =>  'No Evernotes Found',
        'not_found_in_trash' => 'No Evernotes found in Trash', 
        'parent_item_colon' => '',
        'menu_name' => 'Evernotes',
    );
    
    // register post type
    $args = array(
        'labels' => $labels,
        'public' => true,
        'has_archive' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => array('slug' => 'evernotes'),
        'query_var' => true,
        'menu_icon' => 'dashicons-welcome-write-blog',
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'trackbacks',
            'custom-fields',
            'comments',
            'revisions',
            'thumbnail',
            'author',
            'page-attributes'
        )
    );
    register_post_type( 'evernotes', $args );
    
    // register taxonomy
    // register_taxonomy( string $taxonomy, array|string $object_type, array|string $args = array() )
    register_taxonomy('evernote_category', 'evernotes', array('hierarchical' => true, 'label' => 'evernote_category', 'query_var' => true, 'rewrite' => array( 'slug' => 'evernote-category' )));
}
add_action( 'init', 'evernote_init' );