<section class="recipes" id="recipes_filter">
  <h2>Recipes</h2>
  <p>GET COOKING &amp; SHARING</p>
  <div class="recipes_filter">
    <p>Filter By: </p>
    <ul class="recipes_filter_button">
      <li>
        <!-- <a class="recipe_button" href="http://localhost/rahul/wordpress//#recipes_filter">all recipes</a> -->
        <button class="recipe_button active" attr-value="all">all recipes</button>
      </li>
      <li>
        <!-- <a class="recipe_button" href="?recipe_passed=recipe/#recipes_filter">recipe</a> -->
        <button class="recipe_button" attr-value="recipe">recipe</button>
      </li>
      <li>
        <!-- <a class="recipe_button" href="?recipe_passed=learn/#recipes_filter">learn</a> -->
        <button class="recipe_button" attr-value="learn">learn</button>
      </li>
      <li>
        <!-- <a class="recipe_button" href="?recipe_passed=explore/#recipes_filter">explore</a> -->
        <button class="recipe_button" attr-value="explore">explore</button>
      </li>
    </ul>
  </div>
  <div class="recipes_content" id="recipes">
    <ul class="recipes_ul">
      <?php require('recipe_filter_maincode.php'); ?>
    </ul>
    <button class="load_more">Load More</button>

    <?php 
    $count_posts = $query->found_posts;
    if ($count_posts > 5) {
        echo "<script> 
        document.querySelector('.load_more').style.display= 'block';
        </script>";
    } elseif ($count_posts <= 5) {
        echo "<script> 
        document.querySelector('.load_more').style.display= 'none';
        </script>";
    } ?>
  </div>
</section>