<section class="get_inspired">
  <h2>Get inspired</h2>
  <p>HAND PICKED BITS &amp; BITES</p>
  <div class="get_inspired_content">
    <ul class="get_inspired_ul">
      <?php 
          $query = new WP_Query(array(
              'post_type' => 'recipe',
              'recipe_category' => 'get_inspired'
          ));
          // var_dump($query);
          if ( $query->have_posts() ) :                 
      ?>
          <?php while ( $query->have_posts() ) : $query->the_post(); ?>
      <li>            
          <div>
              <img src='<?php the_post_thumbnail_url('smallest'); ?>' alt='hello'>            
              <a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>                
          </div>
      </li>
          <?php endwhile; ?>
      <?php endif; ?>
    </ul>
    <figure class="get_inspired_right">
      <img src="<?= get_template_directory_uri(); ?>/assets/images/advertisement.png" alt="Advertisement">
    </figure>
  </div>
</section>