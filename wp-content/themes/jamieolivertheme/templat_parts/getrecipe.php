<div class="container">

    <!-- Here it starts -->
    <ul class="regular myslider">
        <?php 
            $query = new WP_Query(array(
                'post_type' => 'recipe',
                'recipe_category' => 'recipe'
            ));
            // var_dump($query);
            if ( $query->have_posts() ) :
        ?>
            <?php while ( $query->have_posts() ) : $query->the_post(); ?>
        <li>            
            <div>
                <img class="banner_image" src='<?php the_post_thumbnail_url('smallest'); ?>' alt='hello'>            
                <a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>                
            </div>
        </li>
            <?php endwhile; ?>
        <?php endif; ?>
    </ul>
    <!-- here it ends -->

</div>