<div class="single_container_right">
    <?php if ( have_rows('movies') ) : ?>
        <ul class="siderbar_ul">
            <?php while( have_rows('movies') ): the_row(); ?>
                <li>
                    <figure class="sidebar_image">
                        <img src="<?php the_sub_field('movies_images'); ?>" alt="">                    
                    </figure>
                    <h4><a href="<?php the_sub_field('movie_link'); ?>"><?php the_sub_field('movie_title'); ?></a><h4>
                    <p>contents: <?php the_sub_field('about_movie'); ?></p>
                </li>
                <hr>
            <?php endwhile; ?>
        </ul>
    <?php endif; ?>
</div>