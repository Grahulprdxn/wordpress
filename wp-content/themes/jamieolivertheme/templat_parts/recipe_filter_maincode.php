<?php 
$query = new WP_Query(array(
    'post_type' => 'recipe',
    // 'recipe_category' => $_GET['recipe_passed'],
    'posts_per_page' => 5,
));
// var_dump($query);
if ( $query->have_posts() ) :
?>
    <?php while ( $query->have_posts() ) : $query->the_post(); ?>
<li>            
    <div>
        <img class="recipes_photos" src='<?php the_post_thumbnail_url(); ?>' alt='hello'>            
        <a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>                
    </div>
</li>
    <?php endwhile; ?>
<?php endif; 

wp_reset_query();
?>