<?php get_header(); ?>

<div class="single_container">

    <div class="single_container_left">
        <h1><?php the_title(); ?></h1>


        <!-- IF THE POST HAS THUMBNAIL/FEATURED IMAGE SHOW HERE -->
        <?php if (has_post_thumbnail()):?>

            <img src="<?php the_post_thumbnail_url('largest'); ?>">

        <?php endif; ?>


        <!-- IF THE POST ARE AWAILABLE THEN SHOW -->
        <?php if (have_posts()) : while(have_posts()) : the_post(); ?>

            <?php the_content(); ?>

        <?php endwhile; endif; ?>
    </div>

    <!-- Get recipe from templat_parts folder -->
    <?php 
    // get_template_part( '/templat_parts/sidebar_for_single' ); 
    
    // function setPostViews($postID) {
	//     $count_key = 'post_views_count';
	//     $count = get_post_meta($postID, $count_key, true);
    //     $count++;
    //     // echo $count . "<br>";
    //     update_post_meta($postID, $count_key, $count);
    //     var_dump($count);
    // }
    
    $postID = get_the_ID();
    // setPostViews($postID);
    // echo "hi";
    

// DATE FUNCTION STARTS HERE

// $current_date = current_time('Y/m/d');
$days_ago = date('Y/m/d', strtotime('3 days', strtotime(current_time('Y-m-d'))));
    $current_date = date('Y/m/d', strtotime('12 days', strtotime(current_time('Y-m-d'))));
    echo "Current date: ". $current_date . "<br>";

// delete_post_meta($postID, 'test_meta_key');

$data = get_post_meta( $postID, 'test_meta_key', true);
var_dump($data);

var_dump(array_sum ( $data ));
echo "<br>===============<br>";

// var_dump($current_date);
if (!$data) {
    $subdata = array($current_date => 1);
    add_post_meta( $postID, 'test_meta_key', $subdata ); 
    echo " first entry ";   
} elseif (array_key_exists($current_date, $data)) {
    var_dump($data);
    $data[$current_date] += 1;
    update_post_meta( $postID, 'test_meta_key', $data);
    echo " increament ";
} else {
    $subarray = get_post_meta( $postID, 'test_meta_key', true);
    $subarray[$current_date] = 1;
    // delete_post_meta($postID, 'test_meta_key');
    // var_dump($subarray);
    if (count($subarray) > 5 ) {
        array_shift($subarray);
    }
    update_post_meta( $postID, 'test_meta_key', $subarray );    
    echo " Array push ";
    var_dump(count($data));
}

echo "<br>===============<br>";


$data2 = get_post_meta( $postID, 'test_meta_key', true);
var_dump($data2);
var_dump(array_sum($data2));
$five_day_view_count = array_sum($data2);

delete_post_meta( $postID, 'view_count_5_days' );
update_post_meta( $postID, 'view_count_5_days', $five_day_view_count );

$total_view_count = get_post_meta( $postID, 'view_count_5_days', true);

echo "<br>========<br>";
var_dump($total_view_count);

    ?>

</div>


<?php get_footer(); ?>