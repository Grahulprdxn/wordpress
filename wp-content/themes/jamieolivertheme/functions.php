<?php

// Create Custome Setting for your Theme BY ACF.
// if( function_exists('acf_add_options_page') ) {
	
// 	acf_add_options_page(array(
// 		'page_title' 	=> 'Top Blogs Custom Settings',
// 		'menu_title'	=> 'Top Blogs Settings',
// 		'menu_slug' 	=> 'top-blogs-custom-settings',
// 		'capability'	=> 'edit_posts',
// 		'redirect'		=> false
//     ));
    
// }
// END

// Create Custome Setting for your Theme CUSTOM
function add_admin_page()
{
    add_menu_page( 'Top posts', 'Top Posts Settings', 'manage_options', 'top_posts_setting', 'top_posts_display_page', 'dashicons-admin-settings', 110 );
}

add_action( 'admin_menu', 'add_admin_page' );

function top_posts_display_page()
{
    echo "<h1>Top Posts</h1>";
    require(get_template_directory() . '/templat_parts/top_posts_display_page.php');
}

// END

function load_stylesheets()
{
    // wp_register_style( string $handle, string|bool $src, array $deps = array(), string|bool|null $version = false, string $media = 'all' );

    wp_register_style('mystylesheet', get_template_directory_uri() . '/style.css', array(), null, 'all');
    wp_enqueue_style('mystylesheet');
}
add_action('wp_enqueue_scripts', 'load_stylesheets');

function load_slickstyle()
{
    // wp_register_style( string $handle, string|bool $src, array $deps = array(), string|bool|null $version = false, string $media = 'all' );

    wp_register_style('myslickstyle', get_template_directory_uri() . '/assets/slick-1.8.1/slick/slick.css', array(), null, 'all');
    wp_enqueue_style('myslickstyle');
}
add_action('wp_enqueue_scripts', 'load_slickstyle');

function load_slickthemestyle()
{
    // wp_register_style( string $handle, string|bool $src, array $deps = array(), string|bool|null $version = false, string $media = 'all' );
    wp_register_style( 'Font_Awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css' );
    wp_enqueue_style('Font_Awesome');
 
    wp_register_style('myslickthemestyle', get_template_directory_uri() . '/assets/slick-1.8.1/slick/slick-theme.css', array(), null, 'all');
    wp_enqueue_style('myslickthemestyle');
}
add_action('wp_enqueue_scripts', 'load_slickthemestyle');


function loadjquery()
{
    // wp_register_script( string $handle, string|bool $src, array $deps = array(), string|bool|null $version = false, bool $in_footer = false );
    
    wp_register_script('customjquery', 'https://code.jquery.com/jquery-2.2.0.min.js', $deps = array('jquery'), null, true);
    wp_enqueue_script('customjquery');
}
add_action('wp_enqueue_scripts', 'loadjquery');

function loadjs()
{
    // wp_register_script( string $handle, string|bool $src, array $deps = array(), string|bool|null $version = false, bool $in_footer = false );
    
    wp_register_script('customjs', get_template_directory_uri(). '/assets/js/scripts.js', '', null, true);
    wp_enqueue_script('customjs');
}
add_action('wp_enqueue_scripts', 'loadjs');


function loadslick()
{
    // wp_register_script( string $handle, string|bool $src, array $deps = array(), string|bool|null $version = false, bool $in_footer = false );
    
    wp_register_script('slickimport', get_template_directory_uri(). '/assets/slick-1.8.1/slick/slick.js', '', null, true);
    wp_enqueue_script('slickimport');
}
add_action('wp_enqueue_scripts', 'loadslick');


add_theme_support('menus');

// ADD SUPPORT FOR FEATURED IMAGE
add_theme_support('post-thumbnails');

register_nav_menus(
    array(
        'top-menu' => __('Top Menu', 'theme'),
        'footer-menu' => __('Footer Menu', 'theme')
    )
);

add_image_size('smallest', 300, 300, true);
add_image_size('largest', 800, 800, true);

// FOR AJAX FUNCTIONALITY
function load_ajax() {
    wp_enqueue_script( 'ajax-script', get_template_directory_uri() . '/assets/js/my_ajax_script.js', array('jquery'), null, true );
    wp_localize_script( 'ajax-script', 'my_ajax_object', array( 
        'ajax_url' => admin_url( 'admin-ajax.php' ),
        'nonce' => wp_create_nonce('wp_rest'),
        'siteURL' => get_site_url(),
        'root' => esc_url_raw( rest_url() ),
        ) );
}
add_action( 'wp_enqueue_scripts', 'load_ajax' );

// FOR AJAX LOAD MORE FUNCTIONALITY
add_action( 'wp_ajax_ajax_filter_action', 'ajax_filter_action' );
add_action( 'wp_ajax_nopriv_ajax_filter_action', 'ajax_filter_action' );

function ajax_filter_action() 
{   
    if ($_GET['filtername'] == all) {
        $_GET['filtername'] = '';
    }

    $query = new WP_Query(array(
        'post_type' => 'recipe',
        'recipe_category' => $_GET['filtername'],
        'posts_per_page' => 5,
    ));

    if ( $query->have_posts() ) :
    ?>
        <?php while ( $query->have_posts() ) : $query->the_post(); ?>
    <li>            
        <div>
            <img class="recipes_photos" src='<?php the_post_thumbnail_url(); ?>' alt='hello'>            
            <a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>                
        </div>
    </li>
        <?php endwhile; ?>
    <?php endif;

  die();
}
// END

// FOR AJAX LOAD MORE FUNCTIONALITY
add_action( 'wp_ajax_nonce', 'nonce' );
add_action( 'wp_ajax_nopriv_nonce', 'nonce' );

function nonce()
{   
    $abc = wp_create_nonce();
    echo $abc;
}


// CHECK POST COUNT FUNTION TO DISPLAY LOADMORE BUTTON
add_action( 'wp_ajax_ajax_check_post_count', 'ajax_check_post_count' );
add_action( 'wp_ajax_nopriv_ajax_check_post_count', 'ajax_check_post_count' );

function ajax_check_post_count()
{   
    if ($_GET['filtername'] == all) {
        $_GET['filtername'] = '';
    }

    $query = new WP_Query(array(
        'post_type' => 'recipe',
        'recipe_category' => $_GET['filtername'],
    ));

    $count_posts = $query->found_posts;
    echo $count_posts;
    wp_reset_postdata();
    die();
}
// END

// FOR AJAX LOAD MORE FUNCTIONALITY
add_action( 'wp_ajax_ajax_loadmore_action', 'ajax_loadmore_action' );
add_action( 'wp_ajax_nopriv_ajax_loadmore_action', 'ajax_loadmore_action' );

function ajax_loadmore_action() 
{
    $child_count_ul = $_GET['childcount'];

    if ($_GET['active'] == all) {
        $_GET['active'] = '';
    }

    $query = new WP_Query(array(
        'post_type' => 'recipe',
        'recipe_category' => $_GET['active'],
        'offset' => $child_count_ul,
        'posts_per_page' => 5,
    ));

    if ( $query->have_posts() ) :
    ?>
        <?php while ( $query->have_posts() ) : $query->the_post(); ?>
    <li>            
        <div>
            <img class="recipes_photos" src='<?php the_post_thumbnail_url(); ?>' alt='hello'>
            <a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>
        </div>
    </li>
        <?php endwhile; ?>
    <?php endif;
    wp_reset_postdata();
    die();
}
// END

// FOR AJAX ADDING NEW DATA TO ACF FIELD FUNCTIONALITY
add_action( 'wp_ajax_acfadd', 'acfadd' );
add_action( 'wp_ajax_nopriv_acfadd', 'acfadd' );

function acfadd() 
{   
    $myid = $_GET['postid'];
    $acfvalue = $_GET['acfvalue'];
    update_field('extra_content', $acfvalue, $myid);
    
}

// Recipes Custom Post Type
function recipe_init() {
    // set up product labels
    $labels = array(
        'name' => 'Recipes',
        'singular_name' => 'Recipe',
        'add_new' => 'Add New Recipe',
        'add_new_item' => 'Add New Recipe',
        'edit_item' => 'Edit Recipe',
        'new_item' => 'New Recipe',
        'all_items' => 'All Recipes',
        'view_item' => 'View Recipe',
        'search_items' => 'Search Recipes',
        'not_found' =>  'No Recipes Found',
        'not_found_in_trash' => 'No Recipes found in Trash', 
        'parent_item_colon' => '',
        'menu_name' => 'Recipes',
    );
    
    // register post type
    $args = array(
        'labels' => $labels,
        'public' => true,
        'has_archive' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => array('slug' => 'recipe'),
        'query_var' => true,
        'show_in_rest' => true,
        'menu_icon' => 'dashicons-randomize',
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'trackbacks',
            'custom-fields',
            'comments',
            'revisions',
            'thumbnail',
            'author',
            'page-attributes'
        )
    );
    register_post_type( 'recipe', $args );
    
    // register taxonomy
    // register_taxonomy( string $taxonomy, array|string $object_type, array|string $args = array() )
    register_taxonomy('recipe_category', 'recipe', array('hierarchical' => true, 'label' => 'Category', 'query_var' => true, 'rewrite' => array( 'slug' => 'recipe-category' )));
}
add_action( 'init', 'recipe_init' );

// Restapiresult Custom Post Type
function Restapiresult_init() {
    // set up Restapiresult labels
    $labels = array(
        'name' => 'Restapiresults',
        'singular_name' => 'Restapiresult',
        'add_new' => 'Add New Restapiresult',
        'add_new_item' => 'Add New Restapiresult',
        'edit_item' => 'Edit Restapiresult',
        'new_item' => 'New Restapiresult',
        'all_items' => 'All Restapiresults',
        'view_item' => 'View Restapiresult',
        'search_items' => 'Search Restapiresults',
        'not_found' =>  'No Restapiresults Found',
        'not_found_in_trash' => 'No Restapiresults found in Trash', 
        'parent_item_colon' => '',
        'menu_name' => 'Restapiresults',
    );
    
    // register post type
    $args = array(
        'labels' => $labels,
        'public' => true,
        'has_archive' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'show_in_rest' => true,
        'rewrite' => array('slug' => 'Restapiresult'),
        'query_var' => true,
        'menu_icon' => 'dashicons-clipboard',
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'trackbacks',
            'custom-fields',
            'comments',
            'revisions',
            'thumbnail',
            'author',
            'page-attributes'
        )
    );
    register_post_type( 'Restapiresult', $args );
    
    // register taxonomy
    // register_taxonomy( string $taxonomy, array|string $object_type, array|string $args = array() )
    register_taxonomy('restapiresult_category', 'restapiresult', array('hierarchical' => true, 'label' => 'restapiresult_category', 'query_var' => true, 'rewrite' => array( 'slug' => 'restapiresult-category' )));
}
add_action( 'init', 'Restapiresult_init' );