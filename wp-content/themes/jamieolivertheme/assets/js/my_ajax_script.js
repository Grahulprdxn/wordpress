$( document ).ready(function() {

    // Ajax For loading recipes filter
    $("button.recipe_button").click(function() {
        $(".recipes_filter_button button").removeClass("active");        
        this.classList.add("active");
        var thisvalue = this.getAttribute("attr-value");

        $.ajax({
            type: "get",
            url: my_ajax_object.ajax_url,
            data: {
                filtername: thisvalue,
                action: 'ajax_filter_action',
            },
            success: function(result) {
                $(".recipes_ul").html(result);
                displayfunction(thisvalue);
            }
        });
    });
    
    // FUNCTION FOR LOAD MORE DISPLAY
    function displayfunction(thisvalue) {
        var myul = document.querySelector(".recipes_ul").childElementCount;
        $.ajax({
            type: "get",
            url: my_ajax_object.ajax_url,
            data: {
                ulchildcount: myul,
                filtername: thisvalue,
                action: 'ajax_check_post_count',
            },
            success: function(displayresult) {
                $(".load_more").css("display", myul == displayresult ? 'none' : 'block');
            }
        });
    }

    // FUNCTION FOR LOAD MORE BUTTON
    $(".load_more").click(function(){
        var activevalue = document.querySelector(".recipes_filter_button .active").getAttribute("attr-value");
        var myul = document.querySelector(".recipes_ul").childElementCount;
        $.ajax({
            type: "get",
            url: my_ajax_object.ajax_url,
            data: {
                active: activevalue,
                childcount: myul,
                action: 'ajax_loadmore_action',
            },
            success: function(result) {
                $(".recipes_ul").append(result);
                displayfunction(activevalue);
            }
        });
    });

    // FUNCTION FOR ADDING NEW POSTS IN RESTAPIRESULT
    $(".quick_add_button").click(function() {
        var title = $(".apiresulttitle").val();
        var content = $(".apiresultcontent").val();
        var acfcontent = $(".acfcontent").val();
        
        $.ajax({
            url: my_ajax_object.root + 'wp/v2/restapiresult',
            method: 'POST',
            beforeSend: function ( xhr ) {
                xhr.setRequestHeader( 'X-WP-Nonce', my_ajax_object.nonce );
            },
            data:{
                'title' : title,
                'content': content,
                'status' : 'publish',
            },
        }).done( function ( response ) {
            $o = response;

            $.ajax({
                type: "get",
                url: my_ajax_object.ajax_url,
                data: {
                    postid: $o['id'],
                    action: 'acfadd',
                    acfvalue: acfcontent,
                },
                success: function(result) {
                    console.log('completed');
                }
            });

            $(".apiresulttitle").val('');
            $(".apiresultcontent").val('');
            $(".acfcontent").val('');
        });
    });
});