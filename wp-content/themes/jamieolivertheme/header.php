<!DOCTYPE html>
<html>
    <head>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
        <?php wp_head(); ?>

    </head>
<body <?php body_class(); ?>>

<header>
    <div class="wrapper">
        <h1>
            <figure class="nav-logo">
                <img src="<?= get_template_directory_uri(); ?>/assets/images/logo.png" alt="Jamie Oliver">
            </figure>
        </h1>

        <div class="mymenus">

            <?php wp_nav_menu(
                array(
                    'theme_location' => 'top-menu',
                    'menu_class' => 'navigation'
                )
            ); ?>

        </div>

        <ul class="header_right">
            <li><span>follow</span><span class="menu-down">down</span></li>
            <li><span class="search">search</span></li>
        </ul>
    </div>

</header>