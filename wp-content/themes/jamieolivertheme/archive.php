<?php get_header(); ?>

<div class="container">

    <h1><?php single_cat_title(); ?></h1>
    
    <?php if (have_posts()) : while(have_posts()) : the_post(); ?>

        <h3><?php the_title(); ?></h3>

        <!-- IF THE POST HAS THUMBNAIL SHOW HERE -->
        <?php if (has_post_thumbnail()):?>

            <img src="<?php the_post_thumbnail_url('smallest'); ?>" style="width: 300px; height: auto">

        <?php endif; ?>

        <?php the_excerpt(); ?>
        <a href="<?php the_permalink(); ?>">Read More </a>

    <?php endwhile; endif; ?>

</div>


<?php get_footer(); ?>