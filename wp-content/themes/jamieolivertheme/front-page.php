<?php get_header(); ?>

<!-- Get recipe from templat_parts folder -->
<?php get_template_part( '/templat_parts/getrecipe' ); ?>

<!-- Get inspired from templat_parts folder -->
<?php get_template_part( '/templat_parts/getinspired' ); ?>

<!-- CHECK IF ADMIN IS LOGGED IN THAN ALLOW -->
<?php if (current_user_can('administrator')) : ?>
<!-- Get more_to_explore from templat_parts folder -->
<?php get_template_part( '/templat_parts/apiresult' ); ?>

<?php endif; ?>

<!-- Get more_to_explore from templat_parts folder -->
<?php get_template_part( '/templat_parts/more_to_explore' ); ?>

<!-- Get recipe_filter from templat_parts folder -->
<?php get_template_part( '/templat_parts/recipe_filter' ); ?>

<?php get_footer(); ?>