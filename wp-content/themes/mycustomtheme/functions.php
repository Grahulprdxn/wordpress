<?php

function load_stylesheets()
{
    // wp_register_style( string $handle, string|bool $src, array $deps = array(), string|bool|null $version = false, string $media = 'all' );

    wp_register_style('mystylesheet', get_template_directory_uri() . '/style.css', array(), false, 'all');
    wp_enqueue_style('mystylesheet');
}
add_action('wp_enqueue_scripts', 'load_stylesheets');

function loadjs()
{
    // wp_register_script( string $handle, string|bool $src, array $deps = array(), string|bool|null $version = false, bool $in_footer = false );
    
    wp_register_script('customjs', get_template_directory_uri(). '/js/scripts.js', '', false, true);
    wp_enqueue_script('customjs');
}
add_action('wp_enqueue_scripts', 'loadjs');


add_theme_support('menus');

// ADD SUPPORT FOR FEATURED IMAGE
add_theme_support('post-thumbnails');

register_nav_menus(
    array(
        'top-menu' => __('Top Menu', 'theme'),
        'footer-menu' => __('Footer Menu', 'theme')
    )
);

add_image_size('smallest', 300, 300, true);
add_image_size('largest', 800, 800, true);