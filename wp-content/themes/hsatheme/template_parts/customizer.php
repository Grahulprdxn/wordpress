<?php 

function somename_customize_register( $wp_customize ) {
    
    $wp_customize->add_section('imageoner', array(
        "title" => 'Home Page Images',
        "priority" => 28,
        "description" => __( 'Upload images to display on the homepage.', 'theme-slug' )
    ));
    $wp_customize->add_setting('image_control_one', array(
        'default' => '',
        'type' => 'theme_mod',
        'capability' => 'edit_theme_options',
    ));
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'image_control_one', array(
        'label' => __( 'Featured Home Page Image One', 'theme-slug' ),
        'section' => 'imageoner',
        'settings' => 'image_control_one',
    ))
    );

    // FOR FOOTER TEXT STARTS HERE
    $wp_customize->add_panel('footer_panel',array(
        'title'=>'Footer Panel',
        'description'=> 'Change the content below Footer Logo',
        'priority'=> 10,
    ));

    // ADDRESS STARTS :
    $wp_customize->add_section( 'footer_address_settings', array(
        'title' => 'Footer Address',
        'priority' => 10,
        'panel' => 'footer_panel',
    ) );
    
    $wp_customize->add_setting( 'change_address_control', array(
        'default'  => '',
    ) );

    $wp_customize->add_control( 'change_address_control', array(
        'label' => __( 'Footer Address' ),
        'type' => 'textarea',
        'section' => 'footer_address_settings',
    ) );
    // ADDRESS ENDS
    
    // PHONE NUMBER STARTS :
    $wp_customize->add_section( 'footer_phonenumber_settings', array(
        'title' => 'Footer Phone Number',
        'priority' => 20,
        'panel' => 'footer_panel',
    ) );
    
    $wp_customize->add_setting( 'change_phonenumber_control', array(
        'default'  => '',
    ) );

    $wp_customize->add_control( 'change_phonenumber_control', array(
        'label' => __( 'Footer Phone Number' ),
        'type' => 'text',
        'section' => 'footer_phonenumber_settings',
    ) );
    // PHONE NUMBER ENDS

    // EMAIL ADDRESS STARTS :
    $wp_customize->add_section( 'footer_email_settings', array(
        'title' => 'Footer Email Address',
        'priority' => 30,
        'panel' => 'footer_panel',
    ) );
    
    $wp_customize->add_setting( 'change_email_control', array(
        'default'  => '',
    ) );

    $wp_customize->add_control( 'change_email_control', array(
        'label' => __( 'Footer Email Address' ),
        'type' => 'text',
        'section' => 'footer_email_settings',
    ) );
    // EMAIL ADDRESS ENDS

    // COPYRIGHT STARTS : 
    $wp_customize->add_section( 'footer_copyright_settings', array(
        'title' => 'Footer Copyright',
        'priority' => 40,
        'panel' => 'footer_panel',
    ) );
    
    $wp_customize->add_setting( 'change_copyright_control', array(
        'default'  => '© 2019 Evernote Corporation. All rights reserved.',
    ) );

    $wp_customize->add_control( 'change_copyright_control', array(
        'label' => __( 'Footer Copyright' ),
        'type' => 'text',
        'section' => 'footer_copyright_settings',
    ) );
    // COPYRIGHT ENDS
    // FOOTER ENDS HERE

}
add_action( 'customize_register', 'somename_customize_register' );
?>