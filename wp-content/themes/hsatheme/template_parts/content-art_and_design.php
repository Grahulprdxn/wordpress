<section class="arts_and_design">
    <?php 
    $category_background_image = get_field( "category_background_image", "hsapost_category_36" ); 
    $category_side = get_field("category_show_side", "hsapost_category_36");

    $category_details = $category_side ? "category_details_right" : "category_details_left";
    $post_content = $category_side ? "post_content" : "";

    $category_name = get_terms(array(
        'taxanomy' => 'hsapost_category',
        'slug' => 'art_and_design'
    ));
    // var_dump($category_name);
    ?>
    <figure class="arts_background_image">
        <img src="<?= $category_background_image ?>" alt="Arts $ Design">
    </figure>
    <div class="wrapper">
        <h3><span>HSA</span><span><?= $category_name[0]->name ?></span></h3>
        <!-- <h3><?php
        // $category_name[0]->name 
        ?></h3> -->
        <div class="arts_and_design_content <?= $post_content ?>">
            <div class="<?= $category_details ?>" style="order:<?= $category_side ?>;flex-basis:<?= $flexbasis ?>%;margin-right:<?= $marginright ?>px">
                <p><?= $category_name[0]->description ?></p>
                <a href="<?= get_term_link('art_and_design', 'hsapost_category') ?>">Learn More</a>
            </div>
            <ul class="category_post">
                <?php
                $query = new WP_Query(array(
                    'post_type'  => 'hsaposts',
                    'hsapost_category' => 'art_and_design',
                ));
                while ( $query->have_posts() ) : $query->the_post(); ?>
                <li>            
                    <div>
                        <figure class="post_thmbnail">
                            <img src='<?= the_field('hsa_post_image'); ?>' alt='hello'>
                        </figure>
                        <h2><?= the_title(); ?></h2>
                        <p><?= the_field('hsa_post_content') ?></p>
                        <a href="<?php the_permalink(); ?>" title="<?= the_title() ?>">Learn More</a>                
                    </div>
                </li>
                <?php endwhile; wp_reset_postdata();?>
            </ul>
        </div>
    </div>
</section>