<!DOCTYPE html>
<html>
    <head>

        <?php wp_head(); ?>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    </head>
<body <?php body_class(); ?>>

<header>
    <div class="wrapper">
        <div class="header_left">
            <h1>
                <figure>
                    <a href="<?= get_site_url() ?>" title="HSA">
                        <img src='<?= get_theme_mod('image_control_one') ?>' alt="Evernote">
                    </a>
                </figure>
            </h1>
        </div>
        <div class="header_right">
            <div class="header_right_top">
                <div class="header_right_top_left">
                    <span class='fa fa-globe' aria-hidden='true'></span>
                    <span>EN <i class="fa fa-caret-down" aria-hidden="true"></i></span>
                </div>
                <?php wp_nav_menu(
                    array(
                    'theme_location' => 'simply-social',
                    'menu_class' => 'simply_social',
                    'container' => 'ul'
                    )
                ); ?>
            </div>
            <?php wp_nav_menu(
                array(
                    'theme_location' => 'top-menu',
                    'menu_class' => 'mymenus',
                    'container' => 'ul'
                )
            ); ?>
        </div>
    </div>
    <!-- <div class="wrapper search_section">
        <?php 
        // include(get_template_directory()."/searchform.php"); 
        ?>
    </div> -->

</header>