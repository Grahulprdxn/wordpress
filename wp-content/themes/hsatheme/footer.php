<?php wp_footer(); ?>
<footer>
  <div class="wrapper">
    <div class="footer_top">
      <div class="footer_top_left">
        <h3>
          <figure>
            <img src='<?= get_theme_mod('image_control_one') ?>' alt="Evernote">
          </figure>
        </h3>
        <address><?= get_theme_mod('change_address_control') ?></address>
        <div class="phone_email">
          <a href="#FIXME" title="Call"><?= get_theme_mod('change_phonenumber_control') ?></a>
          <a href="#FIXME" title="Mail"><?= get_theme_mod('change_email_control') ?></a>
        </div>
        <?php wp_nav_menu(
          array(
            'theme_location' => 'simply-social',
            'menu_class' => 'simply_social',
            'container' => 'ul'
          )
        ); ?>
      </div>
      <div class="footer_top_right">
        <div class="footer_menu_one">
          <?php wp_nav_menu(
            array(
                'theme_location' => 'footer-menu_one',
                'menu_class' => 'footer_menus',
                'container' => 'ul'
            )
          ); ?>
        </div>
        <div class="footer_menu_two">
          <?php wp_nav_menu(
            array(
                'theme_location' => 'footer-menu_two',
                'menu_class' => 'footer_menus',
                'container' => 'ul'
            )
          ); ?>
        </div>
        <div class="footer_menu_three">
          <?php wp_nav_menu(
            array(
                'theme_location' => 'footer-menu_three',
                'menu_class' => 'footer_menus',
                'container' => 'ul'
            )
          ); ?>
        </div>
        <div class="footer_menu_four">
          <?php wp_nav_menu(
            array(
                'theme_location' => 'footer-menu_four',
                'menu_class' => 'footer_menus',
                'container' => 'ul'
            )
          ); ?>
        </div>
        <div class="footer_menu_five">
          <?php wp_nav_menu(
            array(
                'theme_location' => 'footer-menu_five',
                'menu_class' => 'footer_menus',
                'container' => 'ul'
            )
          ); ?>
        </div>
        <div class="footer_menu_six">
          <?php wp_nav_menu(
            array(
                'theme_location' => 'footer-menu_six',
                'menu_class' => 'footer_menus',
                'container' => 'ul'
            )
          ); ?>
        </div>
      </div>
    </div>
    <div class="footer_bottom">
      <p><?= get_theme_mod('change_copyright_control') ?></p>
    </div>
  </div>
</footer>

</body>
</html>