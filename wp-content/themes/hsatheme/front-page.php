<?php get_header(); ?>
<main>
    <!-- BANNER SECTION STARTS HERE -->
    <section class="banner">
        <?php 
        // $my_cat = get_field('homepage_relationship');
        // var_dump($my_cat); 
        $images = get_field('banner')["banner_background_images"];
            if ( $images ) : ?>
            <ul>
                <?php foreach( $images as $image ): ?>
                    <li>
                        <figure>
                            <img src="<?= esc_url($image['url']); ?>" alt="Banner Image" />
                        </figure>
                    </li>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>
        <div class="wrapper banner_content">
            <h2><?= get_field('banner')["banner_title"] ?></h2>
        </div>
        <div class="banner_bottom wrapper">
            <ul class="banner_bottom_left">
                <?php while ( have_rows('banner_bottom_content') ) : the_row(); ?>
                <li>
                    <h3><?= the_sub_field('banner_bottom_title') ?></h3>
                    <div class="banner_bottom_content">
                        <?php if (get_sub_field('banner_bottom_date')) : ?>
                        <div class="banner_date">
                            <?php  $date = get_sub_field('banner_bottom_date'); 
                                $time = strtotime($date); 
                                $month = substr(date("F",$time), 0, 3);
                                $day = date("j",$time);
                                $day = strlen($day) < 2 ? "0". $day : $day;
                                ?>
                            <span><?= $day ?></span>
                            <span><?= $month ?></span>
                        </div>
                        <?php endif; ?>
                        <div class="banner_sub_content">
                            <span><?= the_sub_field('banner_bottom_sub_title') ?></span>
                            <p><?= the_sub_field('banner_bottom_content') ?></p>
                        </div>
                    </div>
                </li>        
                <?php endwhile; ?>
            </ul>
            <div class="banner_bottom_right">
                <div class="banner_bottom_right_wrapper">
                    <?php if( have_rows('banner_bottom_content_right') ) : ?>
                    <?php while ( have_rows('banner_bottom_content_right') ) : the_row(); ?>
                        <h3 class="banner_bottom_right_top_header">
                            <span><?= get_sub_field('banner_bottom_right')["banner_bottom_right_year"] ?></span>
                            <span><?= get_sub_field('banner_bottom_right')["banner_bottom_right_title"] ?></span>
                        </h3>
                        <ul>                    
                            <?php while( have_rows('banner_bottom_right_repeater') ): the_row(); ?>
                            <li>
                                <h4><?= the_sub_field('bbr_sub_title') ?></h4>
                                <p><?= the_sub_field('bbr_sub_date'); ?></p>
                            </li>
                            <?php endwhile;?>
                        </ul>
                        <div class="banner_bottom_right_bottom">
                            <h3 class="banner_bottom_right_top_header">
                                <span><?= get_sub_field('rental_spaces')["rental_spaces_title_one"] ?></span>
                                <span><?= get_sub_field('rental_spaces')["rental_spaces_title_two"] ?></span>
                            </h3>
                            <p><?= get_sub_field('rental_spaces')["rental_space_description"] ?></p>
                            <a href="#FIXME" title="Learn More">LEARN MORE</a>
                        </div>
                    <?php endwhile;               
                    endif; ?>
                </div>
            </div>
        </div>
    </section>
    <!-- BANNER SECTION ENDS HERE -->

    <?php 
        $my_cat = get_field('homepage_relationship');
        foreach ($my_cat as $mycat) {
            switch( $mycat->slug )
            {
                case 'music':
                get_template_part('template_parts/content','music');
                break;
                case 'dance':
                get_template_part('template_parts/content','dance');
                break;
                case 'theatre':
                get_template_part('template_parts/content','theatre');
                break;
                case 'art_and_design':
                get_template_part('template_parts/content','art_and_design');
                break;
            }
        }
    ?>
    <section class="sign_up_section">
        <?php include(get_template_directory()."/searchform.php") ?>
    </section>
</main>
<?php get_footer(); ?>