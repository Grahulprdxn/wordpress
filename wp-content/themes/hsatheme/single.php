<?php get_header(); ?>

    <div class="single_container single_wrapper">
        <h1><?php the_title(); ?></h1>
        
        <?php if (get_post_type(get_the_ID()) == speakers) : ?>

            <!-- Get info for events from templat_parts folder -->
            <?php get_template_part( '/templat_parts/singlepageforspeaker' ); ?>

        <?php elseif (get_post_type(get_the_ID()) == sponsors) : ?>

            <!-- Get info for events from templat_parts folder -->
            <?php get_template_part( '/templat_parts/singlepageforsponsors' ); ?>

        <?php else : ?>

            <!-- Get info for events from templat_parts folder -->
            <?php get_template_part( '/templat_parts/singlepageforevent' ); ?>

        <?php endif; ?>

    </div>

<?php get_footer(); ?>