<?php

function load_stylesheets()
{
    // wp_register_style( string $handle, string|bool $src, array $deps = array(), string|bool|null $version = false, string $media = 'all' );

    wp_register_style('mystylesheet', get_template_directory_uri() . '/style.css', array(), null, 'all');
    wp_enqueue_style('mystylesheet');
}
add_action('wp_enqueue_scripts', 'load_stylesheets');

function load_slickstyle()
{
    // wp_register_style( string $handle, string|bool $src, array $deps = array(), string|bool|null $version = false, string $media = 'all' );

    wp_register_style('myslickstyle', get_template_directory_uri() . '/assets/slick-1.8.1/slick/slick.css', array(), null, 'all');
    wp_enqueue_style('myslickstyle');
}
add_action('wp_enqueue_scripts', 'load_slickstyle');

function load_slickthemestyle()
{
    // wp_register_style( string $handle, string|bool $src, array $deps = array(), string|bool|null $version = false, string $media = 'all' );
    wp_register_style( 'Font_Awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css' );
    wp_enqueue_style('Font_Awesome');
 
    wp_register_style('myslickthemestyle', get_template_directory_uri() . '/assets/slick-1.8.1/slick/slick-theme.css', array(), null, 'all');
    wp_enqueue_style('myslickthemestyle');
}
add_action('wp_enqueue_scripts', 'load_slickthemestyle');


function loadjquery()
{
    // wp_register_script( string $handle, string|bool $src, array $deps = array(), string|bool|null $version = false, bool $in_footer = false );
    
    wp_register_script('customjquery', 'https://code.jquery.com/jquery-2.2.0.min.js', $deps = array('jquery'), null, true);
    wp_enqueue_script('customjquery');
}
add_action('wp_enqueue_scripts', 'loadjquery');

function loadjs()
{
    // wp_register_script( string $handle, string|bool $src, array $deps = array(), string|bool|null $version = false, bool $in_footer = false );
    
    wp_register_script('customjs', get_template_directory_uri(). '/assets/js/scripts.js', '', null, true);
    wp_enqueue_script('customjs');
}
add_action('wp_enqueue_scripts', 'loadjs');


function loadslick()
{
    // wp_register_script( string $handle, string|bool $src, array $deps = array(), string|bool|null $version = false, bool $in_footer = false );
    
    wp_register_script('slickimport', get_template_directory_uri(). '/assets/slick-1.8.1/slick/slick.js', '', null, true);
    wp_enqueue_script('slickimport');
}
add_action('wp_enqueue_scripts', 'loadslick');

// FOR AJAX LOAD MORE FUNCTIONALITY
function load_ajax() {
    wp_enqueue_script( 'ajax-script', get_template_directory_uri() . '/assets/js/my_ajax_script.js', array('jquery'), null, true );
    wp_localize_script( 'ajax-script', 'my_ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
}
add_action( 'wp_enqueue_scripts', 'load_ajax' );

add_action( 'wp_ajax_ajax_loadmore_action', 'ajax_loadmore_action_callback' );
add_action( 'wp_ajax_nopriv_ajax_loadmore_action', 'ajax_loadmore_action_callback' );

function ajax_loadmore_action_callback() 
{   
    $child_count_ul = $_GET['childcount'];
    $count_posts = wp_count_posts( 'news' )->publish;
    $page_per_data = $child_count_ul + 1;

    $query = new WP_Query(array(
        'post_type' => 'news',
        'offset' => $child_count_ul,
        'posts_per_page' => 1,
    ));

    // JS FOR HIDING LOAD MORE BUTTON WHEN UL CHILD IS GREATER OR EQUAL TO POSTS COUNT
    if ($child_count_ul >= $count_posts - 1 ) {
        echo "<script>
            document.querySelector('.loadmore').style.display = 'none';
        </script>";
    }

    if ( $query->have_posts() ) :  while ( $query->have_posts() ) : $query->the_post(); 
    ?>
      <li class="newslisting_li">
        <figure class="newslistingleft">
          <img class="newslistingthumbnail" src='<?php the_post_thumbnail_url(); ?>' alt='hello'>
        </figure>
        <div class="newslistingright">

          <!-- DISPLAY TITLE -->
          <a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>

          <!-- DISPLAY THE CATEGORIES -->
          <?php get_template_part( '/templat_parts/displaycategories' ); ?>

          <!-- DISPLAY EXCERPT -->
          <?php the_excerpt(); ?>

          <!-- DISPLAY REPORTER AND EDITOR'S NAME -->
          <?php get_template_part( '/templat_parts/reportereditorname' ); ?>

        </div>
      </li>
  <?php endwhile; endif;
  die();
}
// END


add_theme_support('menus');

// ADD SUPPORT FOR FEATURED IMAGE
add_theme_support('post-thumbnails');

register_nav_menus(
    array(
        'top-menu' => __('Top Menu', 'theme'),
        'footer-menu' => __('Footer Menu', 'theme')
    )
);

// add_image_size('smallest', 300, 300, true);
// add_image_size('largest', 800, 800, true);

// EXCERPT WORD LENGTH TO SHOW
add_filter( 'excerpt_length', function($length) {
    return 20;
} );

// News Custom Post Type
function news_init() {
    // set up news labels
    $labels = array(
        'name' => 'News',
        'singular_name' => 'News',
        'add_new' => 'Add New News',
        'add_new_item' => 'Add New News',
        'edit_item' => 'Edit News',
        'new_item' => 'New News',
        'all_items' => 'All News',
        'view_item' => 'View News',
        'search_items' => 'Search News',
        'not_found' =>  'No News Found',
        'not_found_in_trash' => 'No News found in Trash', 
        'parent_item_colon' => '',
        'menu_name' => 'News',
    );
    
    // register post type
    $args = array(
        'labels' => $labels,
        'public' => true,
        'has_archive' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => array('slug' => 'news'),
        'query_var' => true,
        'menu_icon' => 'dashicons-analytics',
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'trackbacks',
            'custom-fields',
            'comments',
            'revisions',
            'thumbnail',
            'author',
            'page-attributes'
        )
    );
    register_post_type( 'news', $args );
    
    // register taxonomy
    // register_taxonomy( string $taxonomy, array|string $object_type, array|string $args = array() )
    register_taxonomy('news_channel', 'news', array('hierarchical' => true, 'label' => 'Channel', 'query_var' => true, 'rewrite' => array( 'slug' => 'news-category' )));
}
add_action( 'init', 'news_init' );