<section class="newslisting">
  <h2>The News Listing</h2>
  <p>Get All Category News Here</p>
  <div class="newslisting_content wrapper">
    <ul class="newslisting_ul">
      <?php 
        $query = new WP_Query(array(
            'post_type' => 'news',
            'posts_per_page' => 3,
        ));
        // var_dump($query);
        // var_dump($GLOBALS['page_per_data']);
        if ( $query->have_posts() ) :  while ( $query->have_posts() ) : $query->the_post(); 
        ?>
          <li class="newslisting_li">
            <figure class="newslistingleft">
              <img class="newslistingthumbnail" src='<?php the_post_thumbnail_url(); ?>' alt='hello'>
            </figure>
            <div class="newslistingright">

              <!-- DISPLAY TITLE -->
              <a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>

              <!-- DISPLAY THE CATEGORIES -->
              <?php get_template_part( '/templat_parts/displaycategories' ); ?>

              <!-- DISPLAY EXCERPT -->
              <?php the_excerpt(); ?>

              <!-- DISPLAY REPORTER AND EDITOR'S NAME -->
              <?php get_template_part( '/templat_parts/reportereditorname' ); ?>

            </div>
          </li>
      <?php endwhile; endif; ?>
    </ul>
    <div>
        <button class="loadmore">Load More</button>
    </div>
  </div>
</section>