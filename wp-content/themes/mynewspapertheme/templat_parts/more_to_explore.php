<section class="more_to_explore">
  <h2>More to explore</h2>
  <p>TOP FEATURES &amp; VIDEOS</p>
  <div class="more_to_explore_content">
    <ul class="variable more_to_explore_ul">
      <?php 
          $query = new WP_Query(array(
              'post_type' => 'recipe',
              'recipe_category' => 'explore'
          ));
          // var_dump($query);
          if ( $query->have_posts() ) :                 
      ?>
          <?php while ( $query->have_posts() ) : $query->the_post(); ?>
      <li>            
          <div>
              <img src='<?php the_post_thumbnail_url('smallest'); ?>' alt='hello'>            
              <a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>                
          </div>
      </li>
          <?php endwhile; ?>
      <?php endif; ?>
    </ul>
  </div>
</section>