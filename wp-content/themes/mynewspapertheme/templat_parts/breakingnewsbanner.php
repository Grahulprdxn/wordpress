<div class="container">

    <!-- Here it starts -->
    <ul class="regular">
        <?php 
            $query = new WP_Query(array(
                'post_type' => 'news',
                'news_channel' => 'breaking_news'
            ));
            // var_dump($query);
            if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
        <li class="banner_slider_li">            
            <div>
                <img class="banner_slider_image" src='<?php the_post_thumbnail_url('smallest'); ?>' alt='hello'>            
                <a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>                
            </div>
        </li>
        <?php endwhile; endif; ?>
    </ul>
    <!-- here it ends -->

</div>