<div class="reportereditordetails">
    <?php if ( have_rows('editors_details') ) : ?>
        <ul class="editors_detail">
            <?php while( have_rows('editors_details') ): the_row(); ?>
                <li>
                    <figure class="editors_image">
                        <img src="<?php the_sub_field('editors_profile_pic'); ?>" alt="">                    
                    </figure>
                    <div class="editor_name">
                        <h4>Editor's name: <?php the_sub_field('editors_name'); ?></h4>
                        <p>Qualification: <?php the_sub_field('editors_qualification'); ?></p>
                    </div>
                </li>
            <?php endwhile; ?>
        </ul>
    <?php endif; ?>
    <?php if ( have_rows('reporters_details') ) : ?>
        <ul class="reporters_detail">
            <?php while( have_rows('reporters_details') ): the_row(); ?>
                <li>
                    <figure class="reporter_image">
                        <img src="<?php the_sub_field('reporters_profile_pic'); ?>" alt="">                    
                    </figure>
                    <div class="reporter_name">
                        <h4>Reporter's name: <?php the_sub_field('reporters_name'); ?></h4>
                        <p>Qualification: <?php the_sub_field('reporters_qualification'); ?></p>
                    </div>
                </li>
            <?php endwhile; ?>
        </ul>
    <?php endif; ?>
</div>