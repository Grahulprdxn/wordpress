<div class="reportereditorname">
    <?php if ( have_rows('editors_details') ) : ?>
        <ul class="editors_detail">
            <?php while( have_rows('editors_details') ): the_row(); ?>
                <li>
                    <h4>Editor: <?php the_sub_field('editors_name'); ?></h4>
                </li>
            <?php endwhile; ?>
        </ul>
    <?php endif; ?>
    <?php if ( have_rows('reporters_details') ) : ?>
        <ul class="reporters_detail">
            <?php while( have_rows('reporters_details') ): the_row(); ?>
                <li>
                    <h4>Reporter: <?php the_sub_field('reporters_name'); ?></h4>
                </li>
            <?php endwhile; ?>
        </ul>
    <?php endif; ?>
</div>