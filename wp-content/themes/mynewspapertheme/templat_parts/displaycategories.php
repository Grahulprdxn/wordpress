<?php
$terms = get_the_terms( get_the_ID(), 'news_channel' );
            
if ( $terms ) : 
    ?>
<ul class="categories_list">
    <?php
    $channel_links = array();
    
    foreach ( $terms as $term ) { ?>
        <li>
            <a href='<?= get_term_link($term); ?>' style='color:red;' title='$term->name'>
                <?php echo $term->name; ?>
            </a>
        </li>
    <?php }
    ?>
</ul>
<?php endif; ?>