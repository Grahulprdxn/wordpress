<section class="recipes" id="recipes_filter">
  <h2>Recipes</h2>
  <p>GET COOKING &amp; SHARING</p>
  <div class="recipes_filter">
    <p>Filter By: </p>
    <ul class="recipes_filter_button">
      <li>
        <a class="recipe_button" href="http://localhost/rahul/wordpress//#recipes_filter">all recipes</a>
        <!-- <button class="recipe_button" value="all">all recipes</button> -->
      </li>
      <li>
        <a class="recipe_button" href="?recipe_passed=recipe/#recipes_filter">recipe</a>
        <!-- <button class="recipe_button" value="recipe">recipe</button> -->
      </li>
      <li>
        <a class="recipe_button" href="?recipe_passed=learn/#recipes_filter">learn</a>
        <!-- <button class="recipe_button" value="learn">learn</button> -->
      </li>
      <li>
        <a class="recipe_button" href="?recipe_passed=explore/#recipes_filter">explore</a>
        <!-- <button class="recipe_button" value="explore">explore</button> -->
      </li>
    </ul>
  </div>
  <div class="recipes_content" id="recipes">
    <ul class="recipes_ul">
      <?php 
        $query = new WP_Query(array(
            'post_type' => 'recipe',
            'recipe_category' => $_GET['recipe_passed']
        ));
        // var_dump($query);
        if ( $query->have_posts() ) :
      ?>
          <?php while ( $query->have_posts() ) : $query->the_post(); ?>
      <li>            
          <div>
              <img class="recipes_photos" src='<?php the_post_thumbnail_url('smallest'); ?>' alt='hello'>            
              <a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>                
          </div>
      </li>
          <?php endwhile; ?>
      <?php endif; ?>
    </ul>
  </div>
</section>