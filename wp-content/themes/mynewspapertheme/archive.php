<?php get_header(); ?>

<div class="archive_container">

    <h1><?php single_cat_title(); ?></h1>
    
    <?php if (have_posts()) : ?>

        <ul class="archive_wrapper">
            <?php while(have_posts()) : the_post(); ?>
                <li>
                    <figure class="archive_thumbnail">
                        <img src="<?php the_post_thumbnail_url('smallest'); ?>">
                    </figure>

                    <div class="archive_details">
                        <h3><?php the_title(); ?></h3>
                        <?php the_excerpt(); ?>
                        <a href="<?php the_permalink(); ?>">Read More </a>
                    </div>
                </li>
            <?php endwhile;?>
        </ul>
    <?php endif; ?>

</div>


<?php get_footer(); ?>