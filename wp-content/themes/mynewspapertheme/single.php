<?php get_header(); ?>

<div class="single_container">

    <div class="single_container single_wrapper">
        <h1><?php the_title(); ?></h1>

        <!-- DISPLAY THE CATEGORIES -->
        <?php get_template_part( '/templat_parts/displaycategories' ); ?>

        <!-- IF THE POST HAS THUMBNAIL/FEATURED IMAGE SHOW HERE -->
        <?php if (has_post_thumbnail()):?>

            <img class="singlebannerimage" src="<?php the_post_thumbnail_url(); ?>">

        <?php endif; ?>

        <!-- IF THE POST ARE AWAILABLE THEN SHOW -->
        <?php if (have_posts()) : while(have_posts()) : the_post(); ?>

            <?php the_content(); ?>

        <?php endwhile; endif; ?>

        <!-- Get recipe from templat_parts folder -->
        <?php get_template_part( '/templat_parts/reporterandeditordetails' ); ?>

    </div>

</div>


<?php get_footer(); ?>