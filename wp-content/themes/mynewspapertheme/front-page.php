<?php get_header(); ?>

<!-- Get breaking News from templat_parts folder -->
<?php get_template_part( '/templat_parts/breakingnewsbanner' ); ?>

<!-- Get newslisting from templat_parts folder -->
<?php get_template_part( '/templat_parts/newslisting' ); ?>

<?php get_footer(); ?>