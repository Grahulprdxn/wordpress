window.onload = function() {

$(".regular").slick({
    dots: true,
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1
});

$(".variable").slick({
    dots: true,
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1
});

};