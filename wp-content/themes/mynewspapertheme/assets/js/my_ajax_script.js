$(document).ready(function(){
    $(".loadmore").click(function(){
        var myul = document.querySelector(".newslisting_ul").childElementCount;
        $.ajax({
            type: "get",
            url: my_ajax_object.ajax_url,
            data: {
                childcount: myul,
                action: 'ajax_loadmore_action',
            },
            success: function (result) {
                $(".newslisting_ul").append(result);
            }
        });
    });
});