<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

define( 'FS_METHOD' , 'direct' );

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'phpmyadmin' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'c@?|ks?v!;<F>A2AxU|L!C}!]#hI3{TPGbq_peq~gSp#5(LF#H$IKv{DK#zDHLg`' );
define( 'SECURE_AUTH_KEY',  'M/R.jR&,fj;;JBe-V^Ph+W_Z(?>F]-~PihC9=!. Jh6@.Tg%@A,Mp}7Fo;$7jtRU' );
define( 'LOGGED_IN_KEY',    '>+J.l1]?-S8G9[LDW%p]#*/GBhFj30r#;y%]db8kLb0[:mTnu9M]!c=/+LjUJ{|8' );
define( 'NONCE_KEY',        'op)c/J8DdTIK;:]g`?U@/gY;K`I}x~P72Cm))F-Y/PKe[,|DyqxOLV0Wb5`ZO,2e' );
define( 'AUTH_SALT',        'U$*{1x#-,#V}=[Q8w,BE-e&ATMVeQl}aoJS.>Hgp#ONU@3nQf=/&r-P}ock7rs^2' );
define( 'SECURE_AUTH_SALT', 'zsv$C{u,mMzFzc,LU=D5K%#uzo~Jd[{`SY})~J[]T%J+I4=1) %Fld>%Aur;|O/?' );
define( 'LOGGED_IN_SALT',   '|N}/wvkr6^g9vHP*8*m^yLcN(aEGfm09C04inI}o-dEr3Y@C]&4Ku+oA-y(ohFh@' );
define( 'NONCE_SALT',       '^*2t4IzLd[{=n&^RC@vI[EEcx$J0~}V4Ay]:A!RuR%tQ.`*eG8c,F.bCLTYIqiqv' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
